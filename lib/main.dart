import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_application_1/add_user.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: FirebaseOptions(
      apiKey: "AIzaSyBbSUXYSPJWeMJosfK4v6NChKSevsjRbzY",
      authDomain: "module-5-cdc60.firebaseapp.com",
      projectId: "module-5-cdc60",
      storageBucket: "module-5-cdc60.appspot.com",
      messagingSenderId: "963677929333",
      appId: "1:963677929333:web:773c473040b467f3f55d3c",
      measurementId: "G-45MRN6ZCF8",
      databaseURL: "https://module-5-cdc60-default-rtdb.firebaseio.com/"
    ),
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          scaffoldBackgroundColor: Colors.grey[200],
          primarySwatch: Colors.pink,
        ),
        home: MainPage());
  }
}

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return AddUser();
  }
}
