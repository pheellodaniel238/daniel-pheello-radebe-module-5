import 'dart:html';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class userlist extends StatefulWidget {
  const userlist({Key? key}) : super(key: key);

  @override
  State<userlist> createState() => _userlistState();
}

class _userlistState extends State<userlist> {
  final Stream<QuerySnapshot> myUsers =
      FirebaseFirestore.instance.collection("Users").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController usernamefield = TextEditingController();
    TextEditingController phonenumfield = TextEditingController();
    TextEditingController emailText = TextEditingController();
    TextEditingController passwordControler = TextEditingController();
    void delete_user(Doc_id) {
      FirebaseFirestore.instance
          .collection("Users")
          .doc(Doc_id)
          .delete()
          .then((value) => null);
    }

    void eddit_user(data) {
      var information = FirebaseFirestore.instance.collection("Users");
      usernamefield.text = data["Username"];
      phonenumfield.text = data["Phone number"];
      emailText.text = data["Email"];
      passwordControler.text = data["Password"];
      showDialog(
          context: context,
          builder: (_) {
            return AlertDialog(
              title: Text(
                "Eddit User",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.pink,
                ),
              ),
              content: Column(mainAxisSize: MainAxisSize.min, children: [
                TextField(
                  controller: usernamefield,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    labelText: "Username",
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                TextField(
                  controller: phonenumfield,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    labelText: "Phone number",
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                TextField(
                  controller: emailText,
                  decoration: InputDecoration(
                    hintText: "username@gmail.com",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    labelText: "Email",
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                TextField(
                  controller: passwordControler,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    labelText: "Password",
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                TextButton(
                    onPressed: () {
                      information.doc(data["Doc_id"]).update({
                        "Username": usernamefield.text,
                        "Phone number": phonenumfield.text,
                        "Email": emailText.text,
                        "Password": passwordControler.text,
                      });
                      Navigator.pop(context);
                    },
                    child: Text("Update"))
              ]),
            );
          });
    }

    return StreamBuilder(
      stream: myUsers,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Error collecting data");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            // mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: ListView(
                    children: snapshot.data!.docs.map(
                      (DocumentSnapshot documentSnapshot) {
                        Map<String, dynamic> data =
                            documentSnapshot.data()! as Map<String, dynamic>;
                        return Container(
                          margin:
                              EdgeInsets.only(bottom: 10, left: 10, right: 10),
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                              color: Colors.pink[100],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        text: "Username: ",
                                        style: TextStyle(
                                          color: Colors.pink,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        children: [
                                          TextSpan(
                                            text: data['Username'],
                                            style: TextStyle(
                                                color: Colors.pink,
                                                fontSize: 20,
                                                fontWeight: FontWeight.normal),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Text.rich(
                                      TextSpan(
                                        text: "Phone number: ",
                                        style: TextStyle(
                                          color: Colors.pink,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        children: [
                                          TextSpan(
                                            text: data['Phone number'],
                                            style: TextStyle(
                                                color: Colors.pink,
                                                fontSize: 20,
                                                fontWeight: FontWeight.normal),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Text.rich(
                                      TextSpan(
                                        text: "Email: ",
                                        style: TextStyle(
                                          color: Colors.pink,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        children: [
                                          TextSpan(
                                            text: data['Email'],
                                            style: TextStyle(
                                                color: Colors.pink,
                                                fontSize: 20,
                                                fontWeight: FontWeight.normal),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Text.rich(
                                      TextSpan(
                                        text: "Password: ",
                                        style: TextStyle(
                                          color: Colors.pink,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        children: [
                                          TextSpan(
                                            text: data['Password'],
                                            style: TextStyle(
                                                color: Colors.pink,
                                                fontSize: 20,
                                                fontWeight: FontWeight.normal),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ]),
                              ButtonTheme(
                                  child: ButtonBar(
                                children: [
                                  OutlinedButton.icon(
                                    onPressed: (() {
                                      eddit_user(data);
                                    }),
                                    icon: Icon(Icons.edit),
                                    label: Text("Edit"),
                                  ),
                                  OutlinedButton.icon(
                                    onPressed: (() {
                                      delete_user(data['Doc_id']);
                                    }),
                                    icon: Icon(Icons.delete),
                                    label: Text("Delete"),
                                  ),
                                ],
                              )),
                            ],
                          ),
                        );
                      },
                    ).toList(),
                  ),
                ),
              ),
            ],
          );
        } else {
          return (Text("No data"));
        }
      },
    );
  }
}
