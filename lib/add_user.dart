import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_application_1/users.dart';

class AddUser extends StatefulWidget {
  const AddUser({Key? key}) : super(key: key);

  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  String username = "";
  String phonenum = "";
  String email = "";
  String password = "";

  String error_msg = "";
  bool succ = true;

  TextEditingController usernamefield = TextEditingController();
  TextEditingController phonenumfield = TextEditingController();
  TextEditingController emailText = TextEditingController();
  TextEditingController passwordControler = TextEditingController();

  @override
  Widget build(BuildContext context) {
    addUser() {
      final username = usernamefield.text;
      final phonenum = phonenumfield.text;
      final email = emailText.text;
      final password = passwordControler.text;

      if (usernamefield.text == "") {
        setState(() {
          succ = false;
          error_msg = "Enter a valied Username";
        });
      } else if (phonenumfield.text == "") {
        setState(() {
          succ = false;
          error_msg = "Enter a valied Phone number";
        });
      } else if (emailText.text == "") {
        setState(() {
          succ = false;
          error_msg = "Enter a valied Email";
        });
      } else if (passwordControler.text == "") {
        setState(() {
          succ = false;
          error_msg = "Enter a valied Password";
        });
      } else {
        final ref = FirebaseFirestore.instance.collection("Users").doc();

        return ref
            .set({
              "Username": username,
              "Phone number": phonenum,
              "Email": email,
              "Password": password,
              "Doc_id": ref.id,
            })
            .then((value) => setState(() {
                  usernamefield.text = "";
                  phonenumfield.text = "";
                  emailText.text = "";
                  passwordControler.text = "";
                  succ = true;
                  error_msg = "User added successfully";
                }))
            .onError((error, stackTrace) => setState(() {
                  succ = true;
                  error_msg = "User added Unsuccessfully";
                }));
      }
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          const Text(
            "Welcome",
            style: TextStyle(
              fontSize: 50,
              fontWeight: FontWeight.bold,
            ),
          ),
          const Text("Please fill in the missing information to add a user."),
          const SizedBox(
            height: 30,
          ),
          TextField(
            controller: usernamefield,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              labelText: "Username",
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          TextField(
            controller: phonenumfield,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              labelText: "Phone number",
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          TextField(
            controller: emailText,
            decoration: InputDecoration(
              hintText: "username@gmail.com",
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              labelText: "Email",
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          TextField(
            controller: passwordControler,
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              labelText: "Password",
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(
            onPressed: () {
              setState(() {
                addUser();
              });
            },
            child: const Text(
              "Add",
              style: TextStyle(fontSize: 30),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            error_msg,
            style: TextStyle(
              color: succ ? Colors.green : Colors.red,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.italic,
            ),
          ),
          userlist(),
        ]),
      ),
    );
  }
}
